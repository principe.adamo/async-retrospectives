Encoding.default_external = 'UTF-8'

require './lib/description_template'
require './lib/gitlab_api'
require './lib/team'

class Retrospective
  LOG_SEPARATOR = '-' * 70

  attr_reader :token

  def initialize(token)
    @token = token
  end

  def create_issue(team:, dry_run:)
    issue_url = nil
    release = current_release
    template = DescriptionTemplate.new(team, release, token: token)
    result = template.result_with_hash(updating_description: false)

    log_info('Issue', result, dry_run: dry_run)

    unless dry_run
      created_issue = api.post(
        "projects/gl-retrospectives%2F#{team.project}/issues",
        body: {
          title: "#{release} #{team.name} retrospective",
          description: result,
          confidential: team.confidential?
        }
      )

      issue_url = created_issue['_links']['self']

      puts "Created with URL #{issue_url}"
      puts ''
    end

    create_discussions(issue_url, team: team, dry_run: dry_run)
  end

  def mention_team(issue_url, team:, dry_run:)
    comment = Team::TEAM_MEMBER_FIELDS.map do |people|
      next if team[people].nil? || team[people].empty?

      [
        '*',
        *team[people].map { |person| "@#{person}" },
        "(#{people})"
      ].join(' ')
    end.compact.join("\n")

    log_info('Mention comment', comment, dry_run: dry_run)

    unless dry_run
      api.post("#{issue_url}/notes", body: { body: comment })

      puts 'Created comment'
      puts ''
    end
  end

  def create_discussions(issue_url, team:, dry_run:)
    questions = team.discussions.to_a
    questions.unshift(
      'What went well this release?',
      'What didn\'t go well this release?',
      'What can we improve?'
    ) if team.create_discussions

    return if questions.empty?

    questions.each do |question|
      comment = "## #{question}"

      log_info('Discussion', comment, dry_run: dry_run)

      unless dry_run
        api.post("#{issue_url}/discussions", body: { body: comment })
      end
    end

    puts 'Created discussions'
    puts ''
  end

  def create_individual_discussions(issue_url, team:, dry_run:)
    questions = Team::TEAM_MEMBER_FIELDS.map do |people|
      next if team[people].nil? || team[people].empty?

      team[people].map do |person|
        "@#{person} what went well this release? What didn't go well? What can we improve?"
      end
    end.flatten.compact

    questions.each do |question|
      log_info('Discussion', question, dry_run: dry_run)

      unless dry_run
        api.post("#{issue_url}/discussions", body: { body: question })
      end
    end

    puts 'Created individual discussions'
    puts ''
  end

  def update_issue(team:, dry_run:)
    release = current_release(index: -1)
    template = DescriptionTemplate.new(team, release, token: token)
    result = template.result_with_hash(updating_description: true, query_all_groups: team.query_all_groups?)

    log_info("Issue #{template.current_issue_url}", result, verb: 'update', dry_run: dry_run)

    unless dry_run
      api.put(template.current_api_path, body: { description: result })

      puts "Updated issue #{template.current_issue_url}"
      puts ''
    end

    mention_team(template.current_api_path, team: team, dry_run: dry_run) if team.mention_team

    create_individual_discussions(template.current_api_path, team: team, dry_run: dry_run) if team.create_individual_discussions
  end

  def current_release(index: -1)
    today = Time.now.strftime('%Y-%m-%d')
    release = api.get('groups/gitlab-org/milestones?state=active', auth: true)
                 .select { |milestone| milestone['start_date'] && milestone['start_date'] < today }
                 .select { |milestone| /\d+\.\d+/.match?(milestone['title']) }
                 .sort_by { |x| x['start_date'] }[index]['title']
  end

  def log_info(header, message, verb: 'create', dry_run:)
    puts LOG_SEPARATOR
    puts "#{header} to #{verb}#{' (--dry-run enabled)' if dry_run}"
    puts LOG_SEPARATOR
    puts message
    puts LOG_SEPARATOR
    puts ''
  end

  def inspect
    "#<#{self.class.name}:#{object_id}>"
  end

  def api
    @api ||= GitlabApi.new(token: token)
  end
end
