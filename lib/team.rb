require 'yaml'

class Team
  TEAM_MEMBER_FIELDS = [:backend, :frontend, :quality, :ux, :pm, :group]

  FIELDS = [
    :label, :project, :owner, :template, :mention_team,
    :create_discussions, :create_individual_discussions,
    :confidential, :discussions, :query_all_groups,
    *TEAM_MEMBER_FIELDS
  ]

  TeamInfo = Struct.new(:name, *FIELDS) do
    def template
      self[:template] || 'default'
    end

    def confidential?
      !!self[:confidential]
    end

    def query_all_groups?
      !!self[:query_all_groups]
    end
  end

  def self.find(team_name)
    as_team_info(team_name, hash_from_file.fetch(team_name))
  end

  def self.all
    hash_from_file.map(&method(:as_team_info))
  end

  def self.reset!
    @hash_from_file = nil
  end

  private

  def self.hash_from_file
    @hash_from_file ||= YAML.load_file('teams.yml')
  end

  def self.as_team_info(team_name, team_hash)
    TeamInfo.new(team_name, *team_hash.values_at(*FIELDS.map(&:to_s)))
  end
end
