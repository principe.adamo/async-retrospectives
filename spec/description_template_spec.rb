require 'description_template'
require 'team'

RSpec.describe DescriptionTemplate do
  describe '#result_with_hash' do
    ISSUE_FIELDS = ['deliverables', 'slipped', 'follow_up', 'current_retrospective', 'issue_count', 'total_weight', 'merge_request_count']

    let(:team) { Team::TeamInfo.new('backend') }
    let(:erb_template) { ERB.new('', nil, '<>') }
    let(:description_template) { described_class.new(team, '1.0', token: 'token') }

    before do
      allow(description_template).to receive(:load_template).and_return(erb_template)

      ISSUE_FIELDS.each do |field|
        allow(description_template).to receive(field).and_return(field)
      end
    end

    context 'when the hash contains updating_description' do
      ISSUE_FIELDS.each do |field|
        it "includes #{field}" do
          expect(erb_template)
            .to receive(:result_with_hash).with(a_hash_including(field.to_sym))

          description_template.result_with_hash(updating_description: true)
        end
      end
    end

    context 'when the hash does not contain updating_description' do
      ISSUE_FIELDS.each do |field|
        it "does not includes #{field}" do
          expect(erb_template).to receive(:result_with_hash) do |hash|
            expect(hash).not_to include(field.to_sym)
          end

          description_template.result_with_hash(updating_description: false)
        end
      end
    end

    it 'supports nested templates' do
      default_template = ERB.new('<%= include_template.call("foo") %>')
      foo_template = ERB.new('<%= include_template.call("bar") %>')
      bar_template = ERB.new('This is bar: <%= release %>')

      allow(description_template)
        .to receive(:load_template).with('default').and_return(default_template)
      allow(description_template)
        .to receive(:load_template).with('foo').and_return(foo_template)
      allow(description_template)
        .to receive(:load_template).with('bar').and_return(bar_template)

      expect(description_template.result_with_hash({}))
        .to include('This is bar')
        .and include(description_template.release)
    end
  end

  describe 'using the teams in teams.yml' do
    before do
      stub_request(:get, /\A#{Regexp.quote(GitlabApi::ENDPOINT)}/)
        .to_return(
          body: [ { title: 'title', web_url: 'url' } ].to_json,
          headers: { 'Content-Type' => 'application/json' },
        )
    end

    Team.all.each do |team|
      it "renders the #{team.name} team template without errors" do
        expect do
          described_class.new(team, '12.0', token: 'token').result_with_hash(updating_description: false)
          described_class.new(team, '12.0', token: 'token').result_with_hash(updating_description: true)
        end.not_to raise_error
      end
    end
  end
end
